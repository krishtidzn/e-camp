<%-- 
    Document   : addCompany
    Created on : Oct 28, 2015, 9:35:55 PM
    Author     : Kristijan
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" type="text/css" href="css/style.css">
        <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
         <script type="text/javascript" src="js/register.js"></script>
        <script type="text/javascript" src="js/jquery-1.11.0.js"></script>
        <title>Add Company</title>
    </head>
    <body>
        <div id="content">
            <div class="header">
                <div class="product-img">
                    <img class="img-responsive center-block" src="./images/siteDesign/campBanner.png" />                
                </div>
            </div>
            <c:if test="${not empty uname}">

                <c:if test = "${uname eq 'admin'}">
                    <nav class="navbar navbar-default navg" role="navigation">
                        <div class="container">
                            <ul class="nav nav-tabs nav-justified">

                                <li><a href="addCompany.jsp"> Add Company </a></li>   
                                <li><a href="updateCamp.jsp"> Update Camp </a></li>  
                                <li><a href="addEquipment.jsp"> Add Equipment </a></li>  
                                <li><a href="addLocation.jsp"> Add Location </a></li>  
                                <li><a href="addCamp.jsp"> Add Camp </a></li>  
                                <li><a href="addActivity.jsp"> Add Activity </a></li>
                                <li><a href="seeCamps.jsp"> See Camps </a></li>
                                <li><a href="logoutController"> Logout </a> </li>
                            </ul>  
                        </div>
                    </nav>

                </c:if>
            </c:if>
            <form class="form-signin" role="form" method="POST" action="addCompanyController">
                <h2 class="form-signin-heading">Add Company</h2>
                <br>

                <div class="form-element">
                    <label for="cName">Company Name: </label>
                    <input type="text" class="normal" name="cName" id="cName" value="" size="40" 
                           placeholder="Enter the company's name" required >                    
                </div>

                <h4> Locations </h4>
                <div class="form-element">
                    
                    <c:forEach var="location" items="${locations}">
                        <input type="checkbox" name="${location.getLocation()}" id="${location.getLocation()}" values="${location.getLocation()}"> ${location.getLocation()}
                    </c:forEach>
                   
                </div>

                <h4> Equipments </h4>
                <div class="form-element">

                    <c:forEach var="equipment" items="${equipments}">
                        <input type="checkbox" name="${equipment.getEquipment()}" id="${equipment.getEquipment()}" values="${equipment.getEquipment()}"> ${equipment.getEquipment()} 
                    </c:forEach>
               
                </div>

                <div class="form-element">
                    <label for="address">Company Address: </label>
                    <input type="text" class="normal" name="address" id="address" value="" size="40" 
                           placeholder="Enter the company's address" required >                    
                </div>

                <div class="form-element">
                    <label for="cEmail">Email: </label>
                    <input type="cEmail" class="normal" id="cEmail" name="cEmail" placeholder="Enter company's email address" onchange='cEmailValidator()' onfocus='onFocCEmail()' required/>
                    <span id="eEmail"> Please enter valid email  </span> 
                </div>

                <div class="form-element">
                    <label for="cPhoneNo">Telephone: </label>
                    <input type="tel" name="cPhoneNo" class="normal" id="cPhoneNo" size="40" 
                           placeholder="Enter the company's telephone number" required>                    
                </div>




                <div class="form-element">
                    <input type="submit" value="Submit" class="btn btn-lg btn-primary center-block">
                </div>

            </form> 

            <br>
            <div id="footer">
                
            </div>
        </div>

    </body>
</html>
