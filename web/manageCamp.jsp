<%-- 
    Document   : manageCamp
    Created on : Oct 28, 2015, 9:42:05 PM
    Author     : Kristijan
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" type="text/css" href="css/style.css">
        <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
         <script type="text/javascript" src="js/register.js"></script>
        <script type="text/javascript" src="js/jquery-1.11.0.js"></script>
        <title>Manage Camp</title>
    </head>
    <body>
        <div id="content">
            <div class="header">
                <div class="product-img">
                    <img class="img-responsive center-block" src="./images/siteDesign/campBanner.png" />                
                </div>
            </div>
            <c:if test="${not empty uname}">

                <c:if test = "${uname eq 'owner'}">
                    <nav class="navbar navbar-default navg" role="navigation">
                        <div class="container">
                            <ul class="nav nav-tabs nav-justified">

                                <li><a href="manageCamp.jsp"> Manage Camp </a></li>   
                                <li><a href="manageSite.jsp"> Manage Site </a></li>  
                                <li><a href="promoteCamp.jsp"> Promote Camp </a></li>  
                                <li><a href="manageInstructor.jsp"> Manage Instructor </a></li>  
                                <li><a href="hireCampLocation.jsp"> Hire Camp Location </a></li>  
                                <li><a href="hireCampEquipment.jsp"> Hire Camp Equipment </a></li>
                                <li><a href="seeCamps.jsp"> See Camps </a></li>
                                <li><a href="logoutController"> Logout </a> </li>
                            </ul>  
                        </div>
                    </nav>
                </c:if>
            </c:if>
                
            <form class="form-signin" role="form" method="POST" action="manageCampController">
                <h2 class="form-signin-heading">Manage Camp</h2>
                <br>
                <div class="form-element">
                    
                    <c:forEach var="camp" items="${camps}">
                        ${camp.getCampName()} <input type="radio" name="${camp.getCampName()}" id="${camp.getCampName()}" values="${camp.getCampName()}"> 
                    </c:forEach>
                   
                </div>
                
                <div class="form-element">
                    
                    <c:forEach var="activity" items="${activities}">
                        ${activity.getDescription()} <input type="radio" name="${activity.getDescription()}" id="${activity.getDescription()}" values="${activity.getDescription()}"> 
                    </c:forEach>
                   
                </div>
                
                
                <div class="form-element">
                    <input type="submit" value="Link" class="btn btn-lg btn-primary center-block">
                </div>

            </form> 

            <br>
            <div id="footer">
                
            </div>
        </div>
    </body>
</html>

