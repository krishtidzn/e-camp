<%-- 
    Document   : addCamp
    Created on : Oct 28, 2015, 9:34:58 PM
    Author     : Kristijan
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" type="text/css" href="css/style.css">
        <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
         <script type="text/javascript" src="js/register.js"></script>
        <script type="text/javascript" src="js/jquery-1.11.0.js"></script>
        <title>Add Camp</title>
    </head>
    <body>
        <div id="content">
            <div class="header">
                <div class="product-img">
                    <img class="img-responsive center-block" src="./images/siteDesign/campBanner.png" />                
                </div>
            </div>
            <c:if test="${not empty uname}">

                <c:if test = "${uname eq 'admin'}">
                    <nav class="navbar navbar-default navg" role="navigation">
                        <div class="container">
                            <ul class="nav nav-tabs nav-justified">

                                <li><a href="addCompany.jsp"> Add Company </a></li>   
                                <li><a href="updateCamp.jsp"> Update Camp </a></li>  
                                <li><a href="addEquipment.jsp"> Add Equipment </a></li>  
                                <li><a href="addLocation.jsp"> Add Location </a></li>  
                                <li><a href="addCamp.jsp"> Add Camp </a></li>  
                                <li><a href="addActivity.jsp"> Add Activity </a></li>
                                <li><a href="seeCamps.jsp"> See Camps </a></li>
                                <li><a href="logoutController"> Logout </a> </li>
                            </ul>  
                        </div>
                    </nav>

                </c:if>
            </c:if>
            <form class="form-signin" role="form" method="POST" action="addCampController">
                <h2 class="form-signin-heading">Add Camp</h2>
                <br>
                <input type="hidden" name="locationStatus"  id ="locationStatus" value="pending">
                <input type="hidden" name="equipmentStatus"  id ="equipmentStatus" value="pending">
                
                <div class="form-element">
                    <label for="season">Season</label>
                    Winter<input type="radio" name="season" value="winter" required > 
                    Summer<input type="radio" name="season" value="summer" required>  
                </div>
                
                <div class="form-element">
                    <label for="campName">Camp Name: </label>
                    <input type="text" class="normal" name="campName" id="campName" value="" size="40" 
                           placeholder="Enter the camp's name" required >                    
                </div>

                <h4> Locations </h4>
                <div class="form-element">
                    
                    <c:forEach var="location" items="${locations}">
                        ${location.getLocation()} <input type="radio" name="${location.getLocation()}" id="${location.getLocation()}" values="${location.getLocation()}"> 
                    </c:forEach>
                   
                </div>
                
                <div class="form-element">
                    <label for="campPrice">Camp Price: </label>
                    <input type="text" name="campPrice" class="normal" id="campPrice" size="40" 
                           placeholder="Enter camp price" required>  
                    <span> $ </span>
                </div>

                <div class="form-element">
                    <label for="startDate">Start Date: </label>
                    <input type="date" class="normal" name="startDate" id="startDate" value="" size="40" onchange='isSDate()' onfocus='onFocSDate()'
                           placeholder="Enter the camp's start date">
                </div>

                <div class="form-element">
                    <span id='startDateFormat'> The date format is day/month/year; Ex:10/5/2014  </span>  
                </div>
                
                <div class="form-element">
                    <label for="endDate">End Date: </label>
                    <input type="date" class="normal" name="endDate" id="endDate" value="" size="40" onchange='isEDate()' onfocus='onFocEDate()'
                           placeholder="Enter the camp's end date">   

                </div>

                <div class="form-element">
                    <span id='endDateFormat'> The date format is day/month/year; Ex:10/5/2014  </span>  
                </div>
                                

                <div class="form-element">
                    <label for="campParticipantsNo">Participants Number: </label>
                    <input type="text" class="normal" name="campParticipantsNo" id="campParticipantsNo" value="" size="40" 
                           placeholder="Enter the camp's participants number" required >                    
                </div>
      
                <div class="form-element">
                    <input type="submit" value="Submit" class="btn btn-lg btn-primary center-block">
                </div>

            </form> 

            <br>
            <div id="footer">
                
            </div>
        </div>
    </body>
</html>
