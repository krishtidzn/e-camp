<%-- 
    Document   : addActivity
    Created on : Oct 28, 2015, 9:03:33 PM
    Author     : Kristijan
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" type="text/css" href="css/style.css">
        <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
         <script type="text/javascript" src="js/register.js"></script>
        <script type="text/javascript" src="js/jquery-1.11.0.js"></script>
        <title>Add Activity</title>
    </head>
    <body>
        <div id="content">
            <div class="header">
                <div class="product-img">
                    <img class="img-responsive center-block" src="./images/siteDesign/campBanner.png" />                
                </div>
            </div>
            <c:if test="${not empty uname}">

                <c:if test = "${uname eq 'admin'}">
                    <nav class="navbar navbar-default navg" role="navigation">
                        <div class="container">
                            <ul class="nav nav-tabs nav-justified">

                                <li><a href="addCompany.jsp"> Add Company </a></li>   
                                <li><a href="updateCamp.jsp"> Update Camp </a></li>  
                                <li><a href="addEquipment.jsp"> Add Equipment </a></li>  
                                <li><a href="addLocation.jsp"> Add Location </a></li>  
                                <li><a href="addCamp.jsp"> Add Camp </a></li>  
                                <li><a href="addActivity.jsp"> Add Activity </a></li>
                                <li><a href="seeCamps.jsp"> See Camps </a></li>
                                <li><a href="logoutController"> Logout </a> </li>
                            </ul>  
                        </div>
                    </nav>

                </c:if>
            </c:if>
            <form class="form-signin" role="form" method="POST" action="addActivityController">
                <h2 class="form-signin-heading">Add Activity</h2>
                <br>
                
                
                <div class="form-element">
                    <label for="description">Activity Description: </label>
                    <input type="text" class="normal" name="description" id="description" value="" size="40" 
                           placeholder="Enter the activity's description" required >                    
                </div>

<!--                <div class="form-element">
                    <label for="activityParticipantsNo">Participants Number: </label>
                    <input type="text" class="normal" name="activityParticipantsNo" id="activityParticipantsNo" value="" size="40" 
                           placeholder="Enter the activity's participants number" required >                    
                </div>-->
                
                <div class="form-element">
                    <label for="date">Date: </label>
                    <input type="date" class="normal" name="date" id="date" value="" size="40" onchange='isSDate()' onfocus='onFocSDate()'
                           placeholder="Enter the activity's date">
                </div>

                <div class="form-element">
                    <span id='dateFormat'> The date format is day/month/year; Ex:10/5/2014  </span>  
                </div>
                
                <div class="form-element">
                    <label for="activityPrice">Activity Price: </label>
                    <input type="text" name="activityPrice" class="normal" id="activityPrice" size="40" 
                           placeholder="Enter activity price" required>  
                    <span> $ </span>
                </div> 
                      
                <div class="form-element">
                    <input type="submit" value="Submit" class="btn btn-lg btn-primary center-block">
                </div>

            </form> 

            <br>
            <div id="footer">
                
            </div>
        </div>
    </body>
</html>
