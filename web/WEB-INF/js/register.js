/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */








function printValue(sliderID, textbox)
{
    var x = document.getElementById(textbox);
    var y = document.getElementById(sliderID);
    x.value = y.value;
}



function showCNPError(idInput, idError) {
    if (idInput.value.match(document.getElementById('CNP').value)) {
        document.getElementById('eCNP').className = 'errorM';
        document.getElementById('CNP').className = 'errorT';
        document.getElementById('eCNP').innerHTML = 'The CNP has to have 13 digits';

    }

}


function isCNP() {
    document.getElementById('eCNP').className = 'hidden';
    var elem = document.getElementById('CNP');

    var c = elem.value;
    var sex = c.charAt(0);
    var passExp = /^[0-9]{13}$/;
    if (elem.value.match(passExp)) {
        elem.className = 'tbox';
        var val = parseInt(document.getElementById("progress-bar").offsetWidth);
//        var wid = parseInt(document.getElementById("progress-bar").style.width);
        var progress = val + 70;
        document.getElementById("progress-bar").style.width = progress + "px";
        return true;

    } else {
        showCNPError(elem, document.getElementById('eCNP'));
        return false;
    }
}

function onFocCNP() {
    document.getElementById('eCNP').className = 'hidden';
    document.getElementById('CNP').className = 'normal';
}

function onFocEmail() {
    document.getElementById('eEmail').className = 'hidden';
    document.getElementById('email').className = 'normal';
}

function emailValidator() {
    document.getElementById('eEmail').className = 'hidden';
    var elem = document.getElementById('email');


    var emailExp = /^[\w\-\.\+]+\@[a-zA-Z0-9\.\-]+\.[a-zA-z0-9]{2,4}$/;
    if (elem.value.match(emailExp)) {
        document.getElementById('email').className = 'tbox';
        var val = parseInt(document.getElementById("progress-bar").offsetWidth);
//        var wid = parseInt(document.getElementById("progress-bar").style.width);
        var progress = val + 70;
        document.getElementById("progress-bar").style.width = progress + "px";
        return true;
    } else {
        document.getElementById('eEmail').className = 'errorM';
        document.getElementById('email').className = 'errorT';
        document.getElementById('eEmail').innerHTML = 'Wrong Email address.';
        return false;
    }
}

function onFocCEmail() {
    document.getElementById('eEmail').className = 'hidden';
    document.getElementById('cEmail').className = 'normal';
}

function cEmailValidator() {
    document.getElementById('eEmail').className = 'hidden';
    var elem = document.getElementById('cEmail');


    var emailExp = /^[\w\-\.\+]+\@[a-zA-Z0-9\.\-]+\.[a-zA-z0-9]{2,4}$/;
    if (elem.value.match(emailExp)) {
        document.getElementById('cEmail').className = 'tbox';
        var val = parseInt(document.getElementById("progress-bar").offsetWidth);
//        var wid = parseInt(document.getElementById("progress-bar").style.width);
        var progress = val + 70;
        document.getElementById("progress-bar").style.width = progress + "px";
        return true;
    } else {
        document.getElementById('eEmail').className = 'errorM';
        document.getElementById('cEmail').className = 'errorT';
        document.getElementById('eEmail').innerHTML = 'Wrong Email address.';
        return false;
    }
}


function onFocTelNo() {
    document.getElementById('eTel').className = 'hidden';
    document.getElementById('tel').className = 'normal';
}

function isTelNo() {
    document.getElementById('eTel').className = 'hidden';
    var elem = document.getElementById('tel');

    var passExp = /^[0-9]{10}$/;
    if (elem.value.match(passExp)) {
        elem.className = 'tbox';
        var val = parseInt(document.getElementById("progress-bar").offsetWidth);
//        var wid = parseInt(document.getElementById("progress-bar").style.width);
        var progress = val + 70;
        document.getElementById("progress-bar").style.width = progress + "px";
        return true;
    } else {
        document.getElementById('eTel').className = 'errorM';
        document.getElementById('tel').className = 'errorT';
        document.getElementById('eTel').innerHTML = 'The telephone number should have 10 digits';
        return false;
    }
}

function fctAddress() {
    var val = parseInt(document.getElementById("progress-bar").offsetWidth);
//        var wid = parseInt(document.getElementById("progress-bar").style.width);
    var progress = val + 70;
    document.getElementById("progress-bar").style.width = progress + "px";
}


function iban() {
    var val = parseInt(document.getElementById("progress-bar").offsetWidth);
//        var wid = parseInt(document.getElementById("progress-bar").style.width);
    var progress = val + 70;
    document.getElementById("progress-bar").style.width = progress + "px";
}

function isUsername() {
    document.getElementById('euname').className = 'hidden';
    var elem = document.getElementById('uname');

    var alphaExp = /^[a-zA-Z0-9]+$/;
    if (elem.value.match(alphaExp)) {
        document.getElementById('uname').className = 'tbox';
        var val = parseInt(document.getElementById("progress-bar").offsetWidth);
//        var wid = parseInt(document.getElementById("progress-bar").style.width);
        var progress = val + 70;
        document.getElementById("progress-bar").style.width = progress + "px";
        return true;

    } else {
        document.getElementById('euser').className = 'errorM';
        document.getElementById('user').className = 'errorT';
        document.getElementById('euser').innerHTML = 'Wrong User';
        return false;
    }
}

function onFocUser() {
    document.getElementById('euname').className = 'hidden';
    document.getElementById('uname').className = 'normal';
}


function onFocPassword() {
    document.getElementById('epass').className = 'hidden';
    document.getElementById('password').className = 'normal';
}

function onFocRePassword() {
    document.getElementById('erepass').className = 'hidden';
    document.getElementById('repassword').className = 'normal';
}

function isPassword() {
    document.getElementById('epass').className = 'hidden';
    var elem = document.getElementById('password1');


    var passExp = /^[a-zA-Z0-9]{6,30}$/;
    if (elem.value.match(passExp)) {
        document.getElementById('password1').className = 'tbox';
        var val = parseInt(document.getElementById("progress-bar").offsetWidth);
//        var wid = parseInt(document.getElementById("progress-bar").style.width);
        var progress = val + 70;
        document.getElementById("progress-bar").style.width = progress + "px";
        return true;
    } else {
        document.getElementById('epass').className = 'errorM';

        document.getElementById('password1').className = 'errorT';
        document.getElementById('epass').innerHTML = 'The password has to contain at least 6 characters';
        return false;
    }
}

function isRePassword() {
    document.getElementById('erepass').className = 'hidden';

    var elem1 = document.getElementById('password1');
    var elem2 = document.getElementById('repassword');


    if (elem1.value == elem2.value) {
        elem2.className = 'tbox';
        var val = parseInt(document.getElementById("progress-bar").offsetWidth);
//        var wid = parseInt(document.getElementById("progress-bar").style.width);
        var progress = val + 70;
        document.getElementById("progress-bar").style.width = progress + "px";
        return true;
    }
    else {
        document.getElementById('erepass').className = 'errorM';

        document.getElementById('repassword').className = 'errorT';
        document.getElementById('erepass').innerHTML = 'The password has to be identical';
        return false;
    }
}

function formValidator() {


    if (isCNP()) {
        if (isUsername()) {
            if (emailValidator()) {
                if (isPassword()) {
                    if (isRePassword()) {

                        if (isTelNo()) {

                            return true;
                        }

                    }
                }
            }
        }
    }

    return false;
}


function formValidator() {
    if (isUsername()) {
        if (isPassword()) {

            return true;
        }

    }

    return false;
}

function isSDate()
{
    var SDate = document.getElementById('auctionSDate').value;
    if (!/^\d{1,2}\/\d{1,2}\/\d{4}$/.test(SDate))
        document.getElementById('auctionSDate').className = 'errorT';

    var parts = SDate.split("/");
    var day = parseInt(parts[0], 10);
    var month = parseInt(parts[1], 10);
    var year = parseInt(parts[2], 10);


    if (year < 0 || year > 2100 || month == 0 || month > 12)
        document.getElementById('auctionSDate').className = 'errorT';

    var monthLength = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];

    // bisect years
    if (year % 400 == 0 || (year % 100 != 0 && year % 4 == 0))
        monthLength[1] = 29;

    if (day > 0 && day <= monthLength[month - 1])
        document.getElementById('auctionSDate').className = 'normal';
    else {
        document.getElementById('auctionSDate').className = 'errorT';
    }
}

function onFocEDate() {

    document.getElementById('auctionEDate').className = 'normal';
}

function isEDate()
{
    var EDate = document.getElementById('auctionEDate').value;
    if (!/^\d{1,2}\/\d{1,2}\/\d{4}$/.test(EDate))
        document.getElementById('auctionEDate').className = 'errorT';

    var parts = EDate.split("/");
    var day = parseInt(parts[0], 10);
    var month = parseInt(parts[1], 10);
    var year = parseInt(parts[2], 10);


    var SDate = document.getElementById('auctionSDate').value;
    var parts = SDate.split("/");
    var sday = parseInt(parts[0], 10);
    var smonth = parseInt(parts[1], 10);
    var syear = parseInt(parts[2], 10);

    if (sday > day || smonth > month || syear > year)
        document.getElementById('auctionEDate').className = 'errorT';

    if (year < 2014 || year > 2100 || month == 0 || month > 12)
        document.getElementById('auctionEDate').className = 'errorT';

    var monthLength = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];

    // bisect years
    if (year % 400 == 0 || (year % 100 != 0 && year % 4 == 0))
        monthLength[1] = 29;

    if (day > 0 && day <= monthLength[month - 1])
        document.getElementById('auctionSDate').className = 'normal';
    else {
        document.getElementById('auctionEDate').className = 'errorT';
    }
}

function onFocSDate() {

    document.getElementById('auctionSDate').className = 'normal';
}