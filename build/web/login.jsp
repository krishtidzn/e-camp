<%-- 
    Document   : login
    Created on : Oct 28, 2015, 9:41:28 PM
    Author     : Kristijan
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" type="text/css" href="css/style.css">
        <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
        <title>Login</title>
    </head>
    <body>
        <div id="content">
            <div class="header">
                <div class="product-img">
                    <img class="img-responsive center-block" src="./images/siteDesign/campBanner.png" />                
                </div>
            </div>

            <c:if test="${empty uname}">
                <nav class="navbar navbar-default navg" role="navigation">
                    <div class="container">
                        <ul class="nav nav-tabs nav-justified">
                            <li><a href="home.jsp"> Home </a> </li>
                            <li> <a href="register.jsp"> Register </a></li>
                            <li><a href="login.jsp"> Login </a></li>                           
                            <li><a href="seeCamps.jsp"> Go To Camp </a></li>
                            <li><a href="shoppingCart.jsp"> Shopping Cart </a></li>
                            <li><a href="contact.jsp"> Contact </a></li>
                        </ul>   
                    </div>
                </nav>
            </c:if>
            
            <br>
            
            <form method="POST" class="normal, form-signin" action="loginController" role="form">
                 <h2 class="form-signin-heading">Login</h2>
                 <div class="form-element">
                    <label for="uname">Username: </label>
                    <input type="text" name="uname" class="normal" id="uname" size="40" 
                           onchange='isUsername()' onfocus='onFocUser()' placeholder="Enter your username" required>
                    <span id='euname'> </span>
                </div>

                <div class="form-element">
                    <label for="password1">Password: </label>
                    <input type="password" class="normal" name="password1" size="40" id="password1" 
                           onchange='isPassword()' onfocus='onFocPassword()' required placeholder="Please enter your password"  >
                    <!--pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z])\w{6,}" onchange=" this.setCustomValidity(this.validity.patternMismatch ? 'Password must contain at least 6 characters, including UPPER/lowercase and numbers' : ''); 
                        //if(this.checkValidity()) form.pass2.pattern = this.value;" -->
                    <span id='epass'> </span>             
                </div>
                
                <div class="form-element">
                    <input type="submit" class="btn btn-lg btn-primary center-block" value="Submit" onclick='loginFormValidator()'>
                </div>
                
            </form>
            <br>
            <div id="footer">
                
            </div>
    </body>
</html>

