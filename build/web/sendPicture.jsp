<%-- 
    Document   : sendPicture
    Created on : Oct 28, 2015, 9:45:32 PM
    Author     : Kristijan
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" type="text/css" href="css/style.css">
        <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
        <title>Send Picture</title>
    </head>
    <body>       
        <div id="content">
            <div class="header">
                <div class="product-img">
                    <img class="img-responsive center-block" src="./images/siteDesign/campBanner.png" />                
                </div>
            </div>

            <c:if test="${empty uname}">
                <nav class="navbar navbar-default navg" role="navigation">
                    <div class="container">
                        <ul class="nav nav-tabs nav-justified">
                            <li><a href="home.jsp"> Home </a> </li>
                            <li> <a href="register.jsp"> Register </a></li>
                            <li><a href="login.jsp"> Login </a></li>                           
                            <li><a href="seeCamps.jsp"> Go To Camp </a></li>
                            <li><a href="shoppingCart.jsp"> Shopping Cart </a></li>
                            <li><a href="contact.jsp"> Contact </a></li>
                        </ul>   
                    </div>
                </nav>
            </c:if>

            <c:if test="${not empty uname}">

                <c:if test = "${uname eq 'admin'}">
                    <nav class="navbar navbar-default navg" role="navigation">
                        <div class="container">
                            <ul class="nav nav-tabs nav-justified">

                                <li><a href="addCompany.jsp"> Add Company </a></li>   
                                <li><a href="updateCamp.jsp"> Update Camp </a></li>  
                                <li><a href="addEquipment.jsp"> Add Equipment </a></li>  
                                <li><a href="addLocation.jsp"> Add Location </a></li>  
                                <li><a href="addCamp.jsp"> Add Camp </a></li>  
                                <li><a href="addActivity.jsp"> Add Activity </a></li>
                                <li><a href="seeCamps.jsp"> See Camps </a></li>
                                <li><a href="logoutController"> Logout </a> </li>
                            </ul>  
                        </div>
                    </nav>

                </c:if>


                <c:if test = "${uname eq 'owner'}">
                    <nav class="navbar navbar-default navg" role="navigation">
                        <div class="container">
                            <ul class="nav nav-tabs nav-justified">

                                <li><a href="manageCamp.jsp"> Manage Camp </a></li>   
                                <li><a href="manageSite.jsp"> Manage Site </a></li>  
                                <li><a href="promoteCamp.jsp"> Promote Camp </a></li>  
                                <li><a href="manageInstructor.jsp"> Manage Instructor </a></li>  
                                <li><a href="hireCampLocation.jsp"> Hire Camp Location </a></li>  
                                <li><a href="hireCampEquipment.jsp"> Hire Camp Equipment </a></li>
                                <li><a href="seeCamps.jsp"> See Camps </a></li>
                                <li><a href="logoutController"> Logout </a> </li>
                            </ul>  
                        </div>
                    </nav>
                </c:if>

                <c:if test = "${uname.substring(0,4) eq 'inst'}">
                    <nav class="navbar navbar-default navg" role="navigation">
                        <div class="container">
                            <ul class="nav nav-tabs nav-justified">

                                <li><a href="manageScore.jsp"> Manage Score </a></li>   
                                <li><a href="instructorCamps.jsp"> See My Camps </a></li>  
                                <li><a href="logoutController"> Logout </a> </li>
                            </ul>  
                        </div>
                    </nav>
                </c:if>

                <c:if test = "${uname ne 'admin'}">
                    <c:if test = "${uname ne 'owner'}">
                        <c:if test = "${uname.substring(0,4) ne 'inst'}">
                            <nav class="navbar navbar-default navg" role="navigation">
                                <div class="container">
                                    <ul class="nav nav-tabs nav-justified">
                                        <li><a href="sendPicture.jsp">Send Picture</a></li>
                                        <li><a href="seeCamps.jsp"> Go To Camp </a></li>
                                        <li><a href="shoppingCart.jsp"> Shopping Cart </a></li>
                                        <li><a href="contact.jsp"> Contact </a></li>
                                    </ul>  
                                </div>
                            </nav>
                        </c:if>
                    </c:if>
                </c:if>   
            </c:if>    



            <br>


            <form action="sendPictureController" enctype="multipart/form-data" method="POST" class="form-signin text-left" role="form"> 
                <h2 class="form-signin-heading">Send Picture</h2>
                <br>


                <div class="form-element">

                    <c:forEach var="camp" items="${camps}">
                        ${camp.getCampName()} <input type="radio" name="${camp.getCampName()}" id="${camp.getCampName()}" values="${camp.getCampName()}"> 
                    </c:forEach>

                </div>


                <div class="form-element">
                    <input type="file" name="uploadFile" value="Browse"> 
                </div>


                <div class="form-element">
                    <input type="submit" value="Upload" class="btn btn-lg btn-primary center-block">
                </div>




            </form> 
            <br>
            <div id="footer">
                © 2014 NaNoWriMo Bucharest
            </div>
        </div>

    </body>
</html>

