<%-- 
    Document   : home
    Created on : Oct 28, 2015, 9:40:06 PM
    Author     : Kristijan
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" type="text/css" href="css/style.css">
        <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
        <title>NaNoWriMo</title>
    </head>
    <body>
        <div id="content">
            <div class="header">
                <div class="product-img">
                    <img class="img-responsive center-block" src="./images/siteDesign/campBanner.png" />                
                </div>
            </div>

            <c:if test="${empty uname}">
                <nav class="navbar navbar-default navg" role="navigation">
                    <div class="container">
                        <ul class="nav nav-tabs nav-justified">
                            <li><a href="home.jsp"> Home </a> </li>
                            <li> <a href="register.jsp"> Register </a></li>
                            <li><a href="login.jsp"> Login </a></li>                           
                            <li><a href="seeCamps.jsp"> Go To Camp </a></li>
                            <li><a href="shoppingCart.jsp"> Shopping Cart </a></li>
                            <li><a href="contact.jsp"> Contact </a></li>
                        </ul>   
                    </div>
                </nav>
            </c:if>

            <c:if test="${not empty uname}">

                <c:if test = "${uname eq 'admin'}">
                    <nav class="navbar navbar-default navg" role="navigation">
                        <div class="container">
                            <ul class="nav nav-tabs nav-justified">

                                <li><a href="addCompany.jsp"> Add Company </a></li>   
                                <li><a href="updateCamp.jsp"> Update Camp </a></li>  
                                <li><a href="addEquipment.jsp"> Add Equipment </a></li>  
                                <li><a href="addLocation.jsp"> Add Location </a></li>  
                                <li><a href="addCamp.jsp"> Add Camp </a></li>  
                                <li><a href="addActivity.jsp"> Add Activity </a></li>
                                <li><a href="seeCamps.jsp"> See Camps </a></li>
                                <li><a href="logoutController"> Logout </a> </li>
                            </ul>  
                        </div>
                    </nav>

                </c:if>


                <c:if test = "${uname eq 'owner'}">
                    <nav class="navbar navbar-default navg" role="navigation">
                        <div class="container">
                            <ul class="nav nav-tabs nav-justified">

                                <li><a href="manageCamp.jsp"> Manage Camp </a></li>   
                                <li><a href="manageSite.jsp"> Manage Site </a></li>  
                                <li><a href="promoteCamp.jsp"> Promote Camp </a></li>  
                                <li><a href="manageInstructor.jsp"> Manage Instructor </a></li>  
                                <li><a href="hireCampLocation.jsp"> Hire Camp Location </a></li>  
                                <li><a href="hireCampEquipment.jsp"> Hire Camp Equipment </a></li>
                                <li><a href="seeCamps.jsp"> See Camps </a></li>
                                <li><a href="logoutController"> Logout </a> </li>
                            </ul>  
                        </div>
                    </nav>
                </c:if>

                <c:if test = "${uname.substring(0,4) eq 'inst'}">
                    <nav class="navbar navbar-default navg" role="navigation">
                        <div class="container">
                            <ul class="nav nav-tabs nav-justified">

                                <li><a href="manageScore.jsp"> Manage Score </a></li>   
                                <li><a href="instructorCamps.jsp"> See My Camps </a></li>  
                                <li><a href="logoutController"> Logout </a> </li>
                            </ul>  
                        </div>
                    </nav>
                </c:if>

                <c:if test = "${uname ne 'admin'}">
                    <c:if test = "${uname ne 'owner'}">
                        <c:if test = "${uname.substring(0,4) ne 'inst'}">
                            <nav class="navbar navbar-default navg" role="navigation">
                                <div class="container">
                                    <ul class="nav nav-tabs nav-justified">
                                        <li><a href="sendPicture.jsp">Send Picture</a></li>
                                        <li><a href="seeCamps.jsp"> Go To Camp </a></li>
                                        <li><a href="shoppingCart.jsp"> Shopping Cart </a></li>
                                        <li><a href="contact.jsp"> Contact </a></li>
                                    </ul>  
                                </div>
                            </nav>
                        </c:if>
                    </c:if>
                </c:if>   


            </c:if>    



            <br>
            <br>
            <div class="row">
                <!--            <div class="col-lg-6 col-md-6 col-xs-12 col-sm-12 c1">-->
                <div class="col-lg-6 col-md-6 col-xs-12 col-sm-12 c1">
                    <h2>Our Company</h2>
                    <p> Since its establishment in 2010, Camp NaNoWriMo is well-known as the best camp organizer due to our well experience staff, great fun and safety given to your children. It is a trusted company, being on the top level in Romania. Only at us you can find the best price and the best conditions for your child to have the time of life. </p>
                </div>
                <div class="col-lg-6 col-md-6 col-xs-12 col-sm-12 c2">
                    <h2>Why to go on camp?</h2>
                    <p>During the camp every child will enjoy the time of his / her life by spending some time in the nature with their friends, or meeting new ones. Go with NaNoWriMo for having memories for a lifetime!</p>
                </div>
            </div>
            <br>
            <div id="footer">
                
            </div>
        </div>

    </body>
</html>