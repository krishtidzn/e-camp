<%-- 
    Document   : seeCamps
    Created on : Oct 28, 2015, 9:44:09 PM
    Author     : Kristijan
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" type="text/css" href="css/style.css">
        <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
        <script type="text/javascript" src="js/register.js"></script>
        <script type="text/javascript" src="js/jquery-1.11.0.js"></script>
        <script type="text/javascript" src="js/seeCamps.js"></script>
        <title>Go to Camps</title>
    </head>
    <body>

        <div id="content">
            <div class="header">
                <div class="product-img">
                    <img class="img-responsive center-block" src="./images/siteDesign/campBanner.png" />                
                </div>
            </div>

            <c:if test="${empty uname}">
                <nav class="navbar navbar-default navg" role="navigation">
                    <div class="container">
                        <ul class="nav nav-tabs nav-justified">
                            <li><a href="home.jsp"> Home </a> </li>
                            <li> <a href="register.jsp"> Register </a></li>
                            <li><a href="login.jsp"> Login </a></li>                           
                            <li><a href="seeCamps.jsp"> Go To Camp </a></li>
                            <li><a href="shoppingCart.jsp"> Shopping Cart </a></li>
                            <li><a href="contact.jsp"> Contact </a></li>
                        </ul>   
                    </div>
                </nav>
            </c:if>

            <c:if test="${not empty uname}">

                <c:if test = "${uname eq 'admin'}">
                    <nav class="navbar navbar-default navg" role="navigation">
                        <div class="container">
                            <ul class="nav nav-tabs nav-justified">

                                <li><a href="addCompany.jsp"> Add Company </a></li>   
                                <li><a href="updateCamp.jsp"> Update Camp </a></li>  
                                <li><a href="addEquipment.jsp"> Add Equipment </a></li>  
                                <li><a href="addLocation.jsp"> Add Location </a></li>  
                                <li><a href="addCamp.jsp"> Add Camp </a></li>  
                                <li><a href="addActivity.jsp"> Add Activity </a></li>
                                <li><a href="seeCamps.jsp"> See Camps </a></li>
                                <li><a href="logoutController"> Logout </a> </li>
                            </ul>  
                        </div>
                    </nav>

                </c:if>


                <c:if test = "${uname eq 'owner'}">
                    <nav class="navbar navbar-default navg" role="navigation">
                        <div class="container">
                            <ul class="nav nav-tabs nav-justified">

                                <li><a href="manageCamp.jsp"> Manage Camp </a></li>   
                                <li><a href="manageSite.jsp"> Manage Site </a></li>  
                                <li><a href="promoteCamp.jsp"> Promote Camp </a></li>  
                                <li><a href="manageInstructor.jsp"> Manage Instructor </a></li>  
                                <li><a href="hireCampLocation.jsp"> Hire Camp Location </a></li>  
                                <li><a href="hireCampEquipment.jsp"> Hire Camp Equipment </a></li>
                                <li><a href="seeCamps.jsp"> See Camps </a></li>
                                <li><a href="logoutController"> Logout </a> </li>
                            </ul>  
                        </div>
                    </nav>
                </c:if>

                <c:if test = "${uname.substring(0,4) eq 'inst'}">
                    <nav class="navbar navbar-default navg" role="navigation">
                        <div class="container">
                            <ul class="nav nav-tabs nav-justified">

                                <li><a href="manageScore.jsp"> Manage Score </a></li>   
                                <li><a href="instructorCamps.jsp"> See My Camps </a></li>  
                                <li><a href="logoutController"> Logout </a> </li>
                            </ul>  
                        </div>
                    </nav>
                </c:if>

                <c:if test = "${uname ne 'admin'}">
                    <c:if test = "${uname ne 'owner'}">
                        <c:if test = "${uname.substring(0,4) ne 'inst'}">
                            <nav class="navbar navbar-default navg" role="navigation">
                                <div class="container">
                                    <ul class="nav nav-tabs nav-justified">
                                        <li><a href="sendPicture.jsp">Send Picture</a></li>
                                        <li><a href="seeCamps.jsp"> Go To Camp </a></li>
                                        <li><a href="shoppingCart.jsp"> Shopping Cart </a></li>
                                        <li><a href="contact.jsp"> Contact </a></li>
                                    </ul>  
                                </div>
                            </nav>
                        </c:if>
                    </c:if>
                </c:if>   
            </c:if>    

            <form method="post" action="seeCampsController" onchange="" class="form-signin">
                <div class="form-element">
                    <label for="season">Season</label>
                    Winter<input type="radio" name="season" value="winter" required > 
                    Summer<input type="radio" name="season" value="summer" required>                   
                </div>

                <div class="form-element">
                    <input type="submit" value="See Camps" class="btn btn-lg btn-primary center-block">
                </div>    


            </form>

            <c:if test = "${seasonCamps.size() ne 0}"> 
                <form class="form-signin" role="form" method="POST" action="bookCampController">
                    <h2 class="form-signin-heading">Camps</h2>
                    <br>

                    <div class="form-element">

                        <c:forEach var="camp" items="${seasonCamps}">
                            <c:if test = "${camp.getLocationStatus() eq 'done'}"> 
                                <input type="hidden" name="campName" id ="campName" value="${camp.getCampName()}">
                                <p><label for="campName"> Camp Name: </label> ${camp.getCampName()} </p>
                                <p><label for="campParticipantsNo"> Camp Participants No Left: </label> ${camp.getCampParticipantsNo()} </p>
                                <p><label for="campPrice"> Camp Price: </label> ${camp.getCampPrice()} $ </p>
                                <p><label for="startDate"> Camp Start Date: </label> ${camp.getStartDate()} </p>
                                <p><label for="endDate"> Camp End Date: </label> ${camp.getEndDate()} </p>
                                <p><label for="location"> Camp Location: </label> ${camp.location.getLocation()} </p>
                                <p><label for="description"> Description: </label> ${camp.location.getDescription()} </p>
                                
                                <c:forEach var="activity" items="${camp.getActivities()}">
                                    <p><label for="activityDescription"> Activity Description: </label> ${activity.getDescription()} </p>
                                </c:forEach>
                                
                                <p><input type="submit" value="Book" onclick="submit"> </p>
                                
                               
                            </c:if>
                        </c:forEach>

                    </div>



                    <div class="form-element">
                        <input type="submit" value="Submit" class="btn btn-lg btn-primary center-block">
                    </div>

                </form> 
            </c:if>

    </body>
</html>

