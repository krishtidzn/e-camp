<%-- 
    Document   : addLocation
    Created on : Oct 28, 2015, 9:37:07 PM
    Author     : Kristijan
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" type="text/css" href="css/style.css">
        <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
        <title>Add Location</title>
    </head>
    <body>
        <div id="content">
        <div class="header">
                <div class="product-img">
                    <img class="img-responsive center-block" src="./images/siteDesign/campBanner.png" />                
                </div>
            </div>
            <c:if test="${not empty uname}">

                <c:if test = "${uname eq 'admin'}">
                    <nav class="navbar navbar-default navg" role="navigation">
                        <div class="container">
                            <ul class="nav nav-tabs nav-justified">

                                <li><a href="addCompany.jsp"> Add Company </a></li>   
                                <li><a href="updateCamp.jsp"> Update Camp </a></li>  
                                <li><a href="addEquipment.jsp"> Add Equipment </a></li>  
                                <li><a href="addLocation.jsp"> Add Location </a></li>  
                                <li><a href="addCamp.jsp"> Add Camp </a></li>  
                                <li><a href="addActivity.jsp"> Add Activity </a></li>
                                <li><a href="seeCamps.jsp"> See Camps </a></li>
                                <li><a href="logoutController"> Logout </a> </li>
                            </ul>  
                        </div>
                    </nav>

                </c:if>
            </c:if>
            <form class="form-signin" role="form" method="POST" action="addLocationController">
                <h2 class="form-signin-heading">Add Location</h2>

                <div class="form-element">
                    <label for="location">Location: </label>
                    <input type="text" name="location" class="normal" id="location" size="40" 
                           placeholder="Enter new location" required>                   
                </div>

                <div class="form-element">
                    <label for="summerHirePrice">Summer Hire Price: </label>
                    <input type="text" name="summerHirePrice" class="normal" id="summerHirePrice" size="40" 
                           placeholder="Enter summer hire price" required>  
                    <span> $ </span>
                </div>
                
                <div class="form-element">
                    <label for="winterHirePrice">Winter Hire Price: </label>
                    <input type="text" name="winterHirePrice" class="normal" id="winterHirePrice" size="40" 
                           placeholder="Enter winter hire price" required>  
                    <span> $ </span>
                </div>
                
                <div class="form-element">
                    <label for="description">Location Description: </label>
                    <input type="text" name="description" class="normal" id="description" size="40" 
                           placeholder="Enter location description" required>                   
                </div>
                
                <div class="form-element">
                    <label for="capacity">Capacity: </label>
                    <input type="text" name="capacity" class="normal" id="capacity" size="40" 
                           placeholder="Enter location capacity" required>                   
                </div>

                <div class="form-element">
                    <input type="submit" class="btn btn-lg btn-primary center-block" value="Submit" >
                </div>
            </form>     
            <br>
            <div id="footer">
              
            </div>
        </div>
    </body>
</html>

