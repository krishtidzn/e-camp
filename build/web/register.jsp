<%-- 
    Document   : register
    Created on : Oct 28, 2015, 9:43:19 PM
    Author     : Kristijan
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" type="text/css" href="css/style.css">
        <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
        <script type="text/javascript" src="js/register.js"></script>
        <script type="text/javascript" src="js/jquery-1.11.0.js"></script>
        <title>Register to Antiques</title>
    </head>
    <body>
        <div id="content">
            <div class="header">
                <div class="product-img">
                    <img class="img-responsive center-block" src="./images/siteDesign/campBanner.png" />                
                </div>
            </div>

            <c:if test="${empty uname}">
                <nav class="navbar navbar-default navg" role="navigation">
                    <div class="container">
                        <ul class="nav nav-tabs nav-justified">
                            <li><a href="home.jsp"> Home </a> </li>
                            <li> <a href="register.jsp"> Register </a></li>
                            <li><a href="login.jsp"> Login </a></li>                           
                            <li><a href="seeCamps.jsp"> Go To Camp </a></li>
                            <li><a href="shoppingCart.jsp"> Shopping Cart </a></li>
                            <li><a href="contact.jsp"> Contact </a></li>
                        </ul>   
                    </div>
                </nav>
            </c:if>

            <c:if test="${not empty uname}">

                <c:if test = "${uname eq 'admin'}">
                    <nav class="navbar navbar-default navg" role="navigation">
                        <div class="container">
                            <ul class="nav nav-tabs nav-justified">

                                <li><a href="addCompany.jsp"> Add Company </a></li>   
                                <li><a href="updateCamp.jsp"> Update Camp </a></li>  
                                <li><a href="addEquipment.jsp"> Add Equipment </a></li>  
                                <li><a href="addLocation.jsp"> Add Location </a></li>  
                                <li><a href="addCamp.jsp"> Add Camp </a></li>  
                                <li><a href="addActivity.jsp"> Add Activity </a></li>
                                <li><a href="seeCamps.jsp"> See Camps </a></li>
                                <li><a href="logoutController"> Logout </a> </li>
                            </ul>  
                        </div>
                    </nav>

                </c:if>
                
                               
                <c:if test = "${uname eq 'owner'}">
                    <nav class="navbar navbar-default navg" role="navigation">
                        <div class="container">
                            <ul class="nav nav-tabs nav-justified">

                                <li><a href="manageCamp.jsp"> Manage Camp </a></li>   
                                <li><a href="manageSite.jsp"> Manage Site </a></li>  
                                <li><a href="promoteCamp.jsp"> Promote Camp </a></li>  
                                <li><a href="manageInstructor.jsp"> Manage Instructor </a></li>  
                                <li><a href="hireCampLocation.jsp"> Hire Camp Location </a></li>  
                                <li><a href="hireCampEquipment.jsp"> Hire Camp Equipment </a></li>
                                <li><a href="seeCamps.jsp"> See Camps </a></li>
                                <li><a href="logoutController"> Logout </a> </li>
                            </ul>  
                        </div>
                    </nav>
                </c:if>
                
                <c:if test = "${uname.substring(0,4) eq 'inst'}">
                    <nav class="navbar navbar-default navg" role="navigation">
                        <div class="container">
                            <ul class="nav nav-tabs nav-justified">

                                <li><a href="manageScore.jsp"> Manage Score </a></li>   
                                <li><a href="instructorCamps.jsp"> See My Camps </a></li>  
                                <li><a href="logoutController"> Logout </a> </li>
                            </ul>  
                        </div>
                    </nav>
                </c:if>

                <c:if test = "${uname ne 'admin'}">
                    <c:if test = "${uname ne 'owner'}">
                        <c:if test = "${uname.substring(0,4) ne 'inst'}">
                            <nav class="navbar navbar-default navg" role="navigation">
                                <div class="container">
                                    <ul class="nav nav-tabs nav-justified">
                                        <li><a href="sendPicture.jsp">Send Picture</a></li>
                                        <li><a href="seeCamps.jsp"> Go To Camp </a></li>
                                        <li><a href="shoppingCart.jsp"> Shopping Cart </a></li>
                                        <li><a href="contact.jsp"> Contact </a></li>
                                    </ul>  
                                </div>
                            </nav>
                        </c:if>
                    </c:if>
                </c:if>   
            </c:if>    


            <form class="form-signin" role="form" method="POST" action="registerController">
                <h2 class="form-signin-heading">Registration form</h2>
               
                <div class="form-element">
                    <label for="FName">First Name: </label>
                    <input type="text" class="normal" placeholder="Enter your first name" id="FName" name="FName" required aria-required="true" aria-describedby="name-format" />                    
                </div>
                
                <div class="form-element">
                    <label for="LName">Last Name: </label>
                    <input type="text" class="normal" placeholder="Enter your last name" id="LName" name="LName" required aria-required="true" aria-describedby="name-format" />                    
                </div>
                
                <div class="form-element">
                    <label for="CNP">CNP: </label>
                    <input type="text" class="normal" id="CNP" name="CNP" placeholder="Enter your CNP" onchange='isCNP()' onfocus='onFocCNP()' required aria-required="true"/>
                    <span id="eCNP"> Please enter CNP </span>
                </div>
                
                <div class="form-element">
                    <label for="cardIBAN">Card IBAN: </label>
                    <input type="cardIBAN" name="cardIBAN" class="normal" id="cardIBAN" size="40" onchange="iban()"
                           placeholder="Enter your card's IBAN" required>                   
                </div>
                
                <div class="form-element">
                    <label for="tel">Telephone: </label>
                    <input type="tel" name="tel" class="normal" id="tel" size="40" 
                           onchange='isTelNo()' onfocus='onFocTelNo()' placeholder="Enter your telephone number (10 digits)" required>
                    <span id='eTel'> Please enter telephone number of 10 digits </span> 
                </div>
                
                <div class="form-element">
                    <label for="email">Email: </label>
                    <input type="email" class="normal" id="email" name="email" placeholder="Enter your email address" onchange='emailValidator()' onfocus='onFocEmail()' required/>
                    <span id="eEmail"> Please enter valid email  </span> 
                </div>
                            

                <div class="form-element">
                    <label for="address">Address: </label>
                    <input type="address" name="address" class="normal" id="address" size="40" onchange="fctAddress()"
                           placeholder="Enter your address" required>                   
                </div>
                
                <div class="form-element">
                    <label for="schoolName">School Name: </label>
                    <input type="schoolName" name="schoolName" class="normal" id="schoolName" size="40" placeholder="Enter your school name" required>                   
                </div>
                
                <div class="form-element">
                    <label for="schoolAddress">School Address: </label>
                    <input type="schoolAddress" name="schoolAddress" class="normal" id="schoolAddress" size="40" placeholder="Enter your school's address" required>                   
                </div>
                
                <div class="form-element">
                    <label for="schoolCity">School City: </label>
                    <input type="schoolCity" name="schoolCity" class="normal" id="schoolCity" size="40" placeholder="Enter your school's city" required>                   
                </div>
                                
                <div class="form-element">
                    <label for="uname">Username: </label>
                    <input type="text" name="uname" class="normal" id="uname" size="40" 
                           onchange='isUsername()' onfocus='onFocUser()' placeholder="Enter a username" required>
                    <span id='euname'> Please enter a username  </span>
                </div>

                <div class="form-element">
                    <label for="password1">Password: </label>
                    <input type="password" class="normal" name="password1" size="40" id="password1" 
                           onchange='isPassword()' onfocus='onFocPassword()' required placeholder="With upper & lower case ch + at least a digit"  >
                    <!--pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z])\w{6,}" onchange=" this.setCustomValidity(this.validity.patternMismatch ? 'Password must contain at least 6 characters, including UPPER/lowercase and numbers' : ''); 
                        //if(this.checkValidity()) form.pass2.pattern = this.value;" -->
                    <span id='epass'> Please enter password of at least 6 digits </span>             
                </div>

                <div class="form-element">
                    <label for="repassword">Repeat password: </label>
                    <input type="password" class="normal" name="repassword" id="repassword" value="" size="40" 
                           onchange='isRePassword()' onfocus='onFocRePassword()' placeholder="Enter the same password as above" required >
                    <!-- pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z])\w{6,}"
                     onchange=" this.setCustomValidity(this.validity.patternMismatch ? 'Please enter the same Password as above' : ''); " -->
                    <span id='erepass'> Please reenter the same password  </span>  
                </div>

                <div class="form-element">
                    <input type="submit" class="btn btn-lg btn-primary center-block" value="Submit" onclick='formValidator()'>
                </div>
            </form>
            <br>
            <div id="footer">
                
            </div>
        </div>
    </body>
</html>
