/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import domain.Owner;
import javax.persistence.EntityManager;

/**
 *
 * @author Kristijan
 */
public class OwnerDAO {
      protected Class<Owner> entityClass; // so that specific DAO implementations, can access it

    public OwnerDAO(){
        
    }
    
    public void store(EntityManager em, Owner s) {
        em.persist(s);
    }

    public void remove(EntityManager em, Owner s) {
        em.remove(s);
               
    }


    public Owner findById(EntityManager entityManager, Integer id) {
        return entityManager.find(entityClass, id);
    }
}
