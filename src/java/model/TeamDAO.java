/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import domain.Team;
import javax.persistence.EntityManager;

/**
 *
 * @author Kristijan
 */
public class TeamDAO {
     protected Class<Team> entityClass; // so that specific DAO implementations, can access it

    public TeamDAO(){
        
    }
    
    public void store(EntityManager em, Team s) {
        em.persist(s);
    }

    public void remove(EntityManager em, Team s) {
        em.remove(s);
               
    }


    public Team findById(EntityManager entityManager, Integer id) {
        return entityManager.find(entityClass, id);
    }
    
}
