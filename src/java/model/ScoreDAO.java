/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import domain.Score;
import javax.persistence.EntityManager;

/**
 *
 * @author Kristijan
 */
public class ScoreDAO {
     protected Class<Score> entityClass; // so that specific DAO implementations, can access it

    public ScoreDAO(){
        
    }
    
    public void store(EntityManager em, Score s) {
        em.persist(s);
    }

    public void remove(EntityManager em, Score s) {
        em.remove(s);
               
    }


    public Score findById(EntityManager entityManager, Integer id) {
        return entityManager.find(entityClass, id);
    }
    
}
