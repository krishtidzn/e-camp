/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import domain.Camp;
import java.util.List;
import javax.persistence.EntityManager;

/**
 *
 * @author Kristijan
 */
public class CampDAO {
    protected Class<Camp> entityClass; // so that specific DAO implementations, can access it

    public CampDAO(){
        
    }
    
    public void store(EntityManager em, Camp s) {
        em.persist(s);
    }

    public void remove(EntityManager em, Camp s) {
        em.remove(s);
               
    }


    public Camp findById(EntityManager entityManager, Integer id) {
        return entityManager.find(entityClass, id);
    }

    public List<Camp> displayCamps(EntityManager em) {
        List<Camp> c = em.createQuery("SELECT C from Camp C").getResultList();
        return c;
    }

    public Camp getCamp(EntityManager em, String campName) {
        Camp c = (Camp) em.createQuery("SELECT C from Camp C where C.campName=:campName").setParameter("campName", campName).getSingleResult();
        return c;
    }
    
   public Camp updateLocationStatus(EntityManager em, String campName, String locationStatus) {
        Camp c = (Camp) em.createQuery("SELECT C from Camp C where C.campName=:campName").setParameter("campName", campName).getSingleResult();
        c.setLocationStatus(locationStatus);
        return c;
    }

    public List<Camp> displaySeasonCamps(EntityManager em, String season) {
        List<Camp> c = em.createQuery("SELECT C from Camp C where C.season=:season").setParameter("season", season).getResultList();
        return c;
    }
    
}
