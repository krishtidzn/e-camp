/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import domain.Purchase;
import javax.persistence.EntityManager;

/**
 *
 * @author Kristijan
 */
public class PurchaseDAO {
    protected Class<Purchase> entityClass; // so that specific DAO implementations, can access it

    public PurchaseDAO(){
        
    }
    
    public void store(EntityManager em, Purchase s) {
        em.persist(s);
    }

    public void remove(EntityManager em, Purchase s) {
        em.remove(s);
               
    }


    public Purchase findById(EntityManager entityManager, Integer id) {
        return entityManager.find(entityClass, id);
    }
}
