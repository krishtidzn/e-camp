/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import domain.Company;
import javax.persistence.EntityManager;

/**
 *
 * @author Kristijan
 */
public class CompanyDAO {
     protected Class<Company> entityClass; // so that specific DAO implementations, can access it

    public CompanyDAO(){
        
    }
    
    public void store(EntityManager em, Company s) {
        em.persist(s);
    }

    public void remove(EntityManager em, Company s) {
        em.remove(s);
               
    }


    public Company findById(EntityManager entityManager, Integer id) {
        return entityManager.find(entityClass, id);
    }
    
}
