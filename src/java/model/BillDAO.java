/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import domain.Bill;
import javax.persistence.EntityManager;

/**
 *
 * @author Kristijan
 */
public class BillDAO {
     protected Class<Bill> entityClass; // so that specific DAO implementations, can access it

    public BillDAO(){
        
    }
    
    public void store(EntityManager em, Bill s) {
        em.persist(s);
    }

    public void remove(EntityManager em, Bill s) {
        em.remove(s);
               
    }


    public Bill findById(EntityManager entityManager, Integer id) {
        return entityManager.find(entityClass, id);
    }
    
}
