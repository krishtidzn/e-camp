/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import domain.Location;
import java.util.List;
import javax.persistence.EntityManager;

/**
 *
 * @author Kristijan
 */
public class LocationDAO {
     protected Class<Location> entityClass; // so that specific DAO implementations, can access it

    public LocationDAO(){
        
    }
    
    public void store(EntityManager em, Location s) {
        em.persist(s);
    }

    public void remove(EntityManager em, Location s) {
        em.remove(s);
               
    }


    public Location findById(EntityManager entityManager, Integer id) {
        return entityManager.find(entityClass, id);
    }
    
    public List<Location> displayLocations(EntityManager em){
        List<Location> l = em.createQuery("SELECT L from Location L").getResultList();
        return l;
    }
    
    public Location getLocation(EntityManager em, String lo) {
       Location l = (Location) em.createQuery("SELECT L from Location L where L.location=:lo").setParameter("lo", lo).getSingleResult();
       return l;
    }
    
   
    
}
