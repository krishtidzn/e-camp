/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import domain.Activity;
import java.util.List;
import javax.persistence.EntityManager;

/**
 *
 * @author Kristijan
 */
public class ActivityDAO {
    protected Class<Activity> entityClass; // so that specific DAO implementations, can access it

    public ActivityDAO(){
        
    }
    
    public void store(EntityManager em, Activity s) {
        em.persist(s);
    }

    public void remove(EntityManager em, Activity s) {
        em.remove(s);
               
    }


    public Activity findById(EntityManager entityManager, Integer id) {
        return entityManager.find(entityClass, id);
    }

    public List<Activity> displayCamps(EntityManager em) {
        List<Activity> a = em.createQuery("SELECT A from Activity A").getResultList();
        return a;
    }

    public Activity getActivity(EntityManager em, String description) {
        Activity a = (Activity) em.createQuery("SELECT A from Activity A where A.description=:description").setParameter("description", description).getSingleResult();
        return a;
    }
    
}
