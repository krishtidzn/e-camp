/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import domain.School;
import javax.persistence.EntityManager;

/**
 *
 * @author Kristijan
 */
public class SchoolDAO {
     protected Class<School> entityClass; // so that specific DAO implementations, can access it

    public SchoolDAO(){
        
    }
    
    public void store(EntityManager em, School s) {
        em.persist(s);
    }

    public void remove(EntityManager em, School s) {
        em.remove(s);
               
    }


    public School findById(EntityManager entityManager, Integer id) {
        return entityManager.find(entityClass, id);
    }
}
