/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import domain.Picture;
import javax.persistence.EntityManager;

/**
 *
 * @author Kristijan
 */
public class PictureDAO {
    protected Class<Picture> entityClass; // so that specific DAO implementations, can access it

    public PictureDAO(){
        
    }
    
    public void store(EntityManager em, Picture s) {
        em.persist(s);
    }

    public void remove(EntityManager em, Picture s) {
        em.remove(s);
               
    }


    public Picture findById(EntityManager entityManager, Integer id) {
        return entityManager.find(entityClass, id);
    }
    
}
