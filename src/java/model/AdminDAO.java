/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import domain.Admin;
import javax.persistence.EntityManager;

/**
 *
 * @author Kristijan
 */
public class AdminDAO {
     protected Class<Admin> entityClass; // so that specific DAO implementations, can access it

    public AdminDAO(){
        
    }
    
    public void store(EntityManager em, Admin s) {
        em.persist(s);
    }

    public void remove(EntityManager em, Admin s) {
        em.remove(s);
               
    }


    public Admin findById(EntityManager entityManager, Integer id) {
        return entityManager.find(entityClass, id);
    }
    
}
