/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import domain.Hire;
import javax.persistence.EntityManager;

/**
 *
 * @author Kristijan
 */
public class HireDAO {
     protected Class<Hire> entityClass; // so that specific DAO implementations, can access it

    public HireDAO(){
        
    }
    
    public void store(EntityManager em, Hire s) {
        em.persist(s);
    }

    public void remove(EntityManager em, Hire s) {
        em.remove(s);
               
    }


    public Hire findById(EntityManager entityManager, Integer id) {
        return entityManager.find(entityClass, id);
    }
    
}
