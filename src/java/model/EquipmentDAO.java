/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import domain.Equipment;
import java.util.List;
import javax.persistence.EntityManager;

/**
 *
 * @author Kristijan
 */
public class EquipmentDAO {
    protected Class<Equipment> entityClass; // so that specific DAO implementations, can access it

    public EquipmentDAO(){
        
    }
    
    public void store(EntityManager em, Equipment s) {
        em.persist(s);
    }

    public void remove(EntityManager em, Equipment s) {
        em.remove(s);
               
    }


    public Equipment findById(EntityManager entityManager, Integer id) {
        return entityManager.find(entityClass, id);
    }
    
    public List<Equipment> displayEquipments(EntityManager em){
        List<Equipment> e = em.createQuery("SELECT E from Equipment E").getResultList();
        return e;
    }

    public Equipment getEquipment(EntityManager em, String eq) {
       Equipment e = (Equipment) em.createQuery("SELECT E from Equipment E where E.equipment=:eq").setParameter("eq", eq).getSingleResult();
       return e;
    }
    
}
