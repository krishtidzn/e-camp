/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import domain.Participant;
import javax.persistence.EntityManager;

/**
 *
 * @author Kristijan
 */
public class ParticipantDAO {
     protected Class<Participant> entityClass; // so that specific DAO implementations, can access it

    public ParticipantDAO(){
        
    }
    
    public void store(EntityManager em, Participant participant) {
        em.persist(participant);
    }

    public void remove(EntityManager em, Participant entity) {
        em.remove(entity);
               
    }


    public Participant findById(EntityManager entityManager, Integer id) {
        return entityManager.find(entityClass, id);
    }

    
    public Participant retrieveParticipant(EntityManager em, String usrname) {
       Participant p = (Participant) em.createQuery("SELECT P from Participant P where P.registration.username=:username").setParameter("username", usrname).getSingleResult();
       return p;
    }
    
}
