/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import domain.Instructor;
import javax.persistence.EntityManager;

/**
 *
 * @author Kristijan
 */
public class InstructorDAO {
    protected Class<Instructor> entityClass; // so that specific DAO implementations, can access it

    public InstructorDAO(){
        
    }
    
    public void store(EntityManager em, Instructor s) {
        em.persist(s);
    }

    public void remove(EntityManager em, Instructor s) {
        em.remove(s);
               
    }


    public Instructor findById(EntityManager entityManager, Integer id) {
        return entityManager.find(entityClass, id);
    }
    
}
