/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import domain.Registration;
import java.util.List;
import javax.persistence.EntityManager;

/**
 *
 * @author Kristijan
 */
public class RegistrationDAO {
    protected Class<Registration> entityClass; // so that specific DAO implementations, can access it

    public RegistrationDAO(){
        
    }
    
    public void store(EntityManager em, Registration s) {
        em.persist(s);
    }

    public void remove(EntityManager em, Registration s) {
        em.remove(s);
               
    }


    public Registration findById(EntityManager entityManager, Integer id) {
        return entityManager.find(entityClass, id);
    }

    
    public boolean checkUsername(EntityManager em, String usrname) {
       List<Registration> reg = em.createQuery("SELECT R from Registration R where R.username=:username").setParameter("username", usrname).getResultList();
                    
           
           if(reg.size() != 0){
               return true;
             
           }
           return false;
    }

    public String findPassword(EntityManager em, String usern) {
        Registration reg = (Registration) em.createQuery("SELECT R from Registration R where R.username=:usern").setParameter("usern", usern).getSingleResult();
        return reg.getPassword();
    }

    public Registration getParticipant(EntityManager em, String username) {
       Registration reg = (Registration) em.createQuery("SELECT R from Registration R where R.username=:usern").setParameter("usern", username).getSingleResult();
       return reg;
    }
}
