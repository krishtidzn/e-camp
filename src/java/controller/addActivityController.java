/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import domain.Activity;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.ActivityDAO;

/**
 *
 * @author Kristijan
 */
@WebServlet(name = "addActivityController", urlPatterns = {"/addActivityController"})
public class addActivityController extends HttpServlet {
    private ActivityDAO activityDAO = new ActivityDAO();
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
         response.setContentType("text/html;charset=UTF-8");
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("SDM_ProjectPU");
        EntityManager em = emf.createEntityManager();
        try (PrintWriter out = response.getWriter()) {
            String description = request.getParameter("description");
            System.out.println("The activity's description is " + description);
            int activityParticipantsNo = 0;
            
            double activityPrice = Double.parseDouble(request.getParameter("activityPrice"));
            

            Activity a = new Activity();
            a.setDescription(description);
            a.setActivityParticipantsNo(activityParticipantsNo);
            a.setActivityPrice(activityPrice);
                     

            String date = request.getParameter("date");
            System.out.println("Date: " + date);
            String Dat;

            String parts[] = date.split("/");
            String sday = parts[0];
            String smonth = parts[1];
            String syear = parts[2];
            SimpleDateFormat sdf = new SimpleDateFormat("dd/M/yyyy");
            Calendar calendar = new GregorianCalendar(Integer.parseInt(syear), Integer.parseInt(smonth) - 1, Integer.parseInt(sday));
            Dat = sdf.format(calendar.getTime());
            System.out.println("Start date: " + Dat);
            a.setDate(date);
            
            em.getTransaction().begin();

            try {

                activityDAO.store(em, a);
                em.getTransaction().commit();
            } catch (Exception ex) {
                ex.printStackTrace();
                em.getTransaction().rollback();
            } finally {
                em.close();

            }
            out.println("<script language='javascript'>alert('Added activity!');"
                    + "self.location='home.jsp';</script>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
