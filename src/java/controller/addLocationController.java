/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import domain.Location;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.LocationDAO;

/**
 *
 * @author Kristijan
 */
@WebServlet(name = "addLocationController", urlPatterns = {"/addLocationController"})
public class addLocationController extends HttpServlet {

    private LocationDAO locationDAO = new LocationDAO();
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("SDM_ProjectPU");
        EntityManager em = emf.createEntityManager();
        try (PrintWriter out = response.getWriter()) {
            String location = request.getParameter("location");
            System.out.println("The location to be added is " + location);
            Double summerHirePrice = Double.parseDouble(request.getParameter("summerHirePrice"));
            Double winterHirePrice = Double.parseDouble(request.getParameter("winterHirePrice"));
            String description = request.getParameter("description");
            int capacity = Integer.parseInt(request.getParameter("capacity"));
            Location loc = new Location();
            loc.setLocation(location);
            loc.setSummerHirePrice(summerHirePrice);
            loc.setWinterHirePrice(winterHirePrice);
            loc.setDescription(description);
            loc.setCapacity(capacity);
            
            List locs= locationDAO.displayLocations(em);
            if (locs.size()!=0) {
                locs.add(loc);
                request.getSession().setAttribute("locations", locs);
            }
            
            em.getTransaction().begin();

            try {

                locationDAO.store(em, loc);
                em.getTransaction().commit();
            } catch (Exception ex) {
                ex.printStackTrace();
                em.getTransaction().rollback();
            } finally {
                em.close();

            }
            out.println("<script language='javascript'>alert('Added location!');"
                    + "self.location='home.jsp';</script>");
        
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
