/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import domain.Camp;
import domain.Participant;
import domain.Picture;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import static java.lang.System.out;
import java.util.List;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;
import model.CampDAO;
import model.ParticipantDAO;
import model.PictureDAO;

/**
 *
 * @author Kristijan
 */
@WebServlet(name = "sendPictureController", urlPatterns = {"/sendPictureController"})
public class sendPictureController extends HttpServlet {
    
    private CampDAO campDAO = new CampDAO();
    private PictureDAO pictureDAO = new PictureDAO();
    private ParticipantDAO participantDAO = new ParticipantDAO();
    EntityManagerFactory emf = Persistence.createEntityManagerFactory("SDM_ProjectPU");
    EntityManager em = emf.createEntityManager();

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    private static final Logger logger = Logger.getLogger(sendPictureController.class.getName());

    public sendPictureController() {
        super();
    }

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        HttpSession session = request.getSession();
        String fileName = null;
        for (Part part : request.getParts()) {
            //logger.info(part.getName());
            InputStream is = request.getPart(part.getName()).getInputStream();
            int i = is.available();
            byte[] b = new byte[i];
            is.read(b);
            //logger.info("Length : " + b.length);
            fileName = getFileName(part);
            //logger.info("File name : " + fileName);
            FileOutputStream os = new FileOutputStream("C:/Users/User/My Documents/NetBeansProjects/SDM_Project/build/web/images/camp/" + fileName);
            os.write(b);
            is.close();

        }

        em.getTransaction().begin();

        Camp camp = null;
        List<Camp> cmps = (List) request.getSession().getAttribute("camps");
        String cp;
        for (int i = 0; i < cmps.size(); i++) {
            cp = request.getParameter(cmps.get(i).getCampName());
            if (cp != null) { //it means that the checkbox is checked
                System.out.println("The camp wanted is " + cmps.get(i).getCampName());
                camp = campDAO.getCamp(em, cmps.get(i).getCampName());
            }
        }
        
        String uname = session.getAttribute("uname").toString();
        Participant p = participantDAO.retrieveParticipant(em, uname);
        p.setCamp(camp);
        camp.setParticipant(p);
        String imageName = fileName;
        Picture img = new Picture();
        img.setPictureName(imageName);
        img.setCamp(camp);

        img.setParticipant(p);
        img.setCamp(camp);
        camp.getPictures().add(img);

        try {
            pictureDAO.store(em, img);
            em.getTransaction().commit();
        } catch (Exception ex) {
            ex.printStackTrace();
            em.getTransaction().rollback();
        } finally {
            em.close();

        }
        ServletContext sc = getServletContext();
        RequestDispatcher rd = sc.getRequestDispatcher("/home.jsp");
        rd.forward(request, response);
        out.println("<script language='javascript'>alert('Added picture!');"
                + "self.location='http://localhost:9999/SDM_Project/home.jsp';</script>");
    }
    

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>


private String getFileName(Part part) { //gets the file name from the content-disposition header
        String partHeader = part.getHeader("content-disposition");
        // logger.info("Part Header = " + partHeader);
        for (String cd : part.getHeader("content-disposition").split(";")) {
            if (cd.trim().startsWith("filename")) {
                return cd.substring(cd.indexOf('=') + 1).trim()
                        .replace("\"", "");
            }
        }
        return null;
    }

}
