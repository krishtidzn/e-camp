/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import domain.Camp;
import domain.Location;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.CampDAO;
import model.HireDAO;
import model.LocationDAO;

/**
 *
 * @author Kristijan
 */
@WebServlet(name = "hireCampLocationController", urlPatterns = {"/hireCampLocationController"})
public class hireCampLocationController extends HttpServlet {

    private CampDAO campDAO = new CampDAO();
    private LocationDAO locationDAO = new LocationDAO();
    private HireDAO hireDAO = new HireDAO();
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("SDM_ProjectPU");
        EntityManager em = emf.createEntityManager();
        try (PrintWriter out = response.getWriter()) {
            em.getTransaction().begin();
            List<Location> locats = (List) request.getSession().getAttribute("locations");
            String lo;
            Location locat = null;
            for (int i = 0; i < locats.size(); i++) {
                lo = request.getParameter(locats.get(i).getLocation());
                if (lo != null) { //it means that the checkbox is checked
                    System.out.println("The location wanted is " + locats.get(i).getLocation());
                    locat = locationDAO.getLocation(em, locats.get(i).getLocation());
                    System.out.println("Location Status "+request.getParameter("locationStatus") );
                    System.out.println("Found "+locat.getCamp().getLocationStatus());
                    locat.getCamp().setLocationStatus(request.getParameter("locationStatus"));
                    System.out.println("After "+locat.getCamp().getLocationStatus());
                }
            }
            Camp c = em.merge(campDAO.updateLocationStatus(em, locat.getCamp().getCampName(), request.getParameter("locationStatus") ));
            em.getTransaction().commit();
            em.close();
            out.println("<script language='javascript'>alert('Done location hiring!');"
                    + "self.location='home.jsp';</script>");
        }
    }


    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
