/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import domain.Camp;
import domain.Location;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.CampDAO;
import model.LocationDAO;

/**
 *
 * @author Kristijan
 */
@WebServlet(name = "addCampController", urlPatterns = {"/addCampController"})
public class addCampController extends HttpServlet {

    private LocationDAO locationDAO = new LocationDAO();
    private CampDAO campDAO = new CampDAO();

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("SDM_ProjectPU");
        EntityManager em = emf.createEntityManager();
        try (PrintWriter out = response.getWriter()) {
            String season = request.getParameter("season");
            System.out.println("The camp's season is " + season);
            String campName = request.getParameter("campName");
            double campPrice = Double.parseDouble(request.getParameter("campPrice"));
            String locationStatus = request.getParameter("locationStatus");
            int campParticipantsNo = Integer.parseInt(request.getParameter("campParticipantsNo"));
            String equipmentStatus = request.getParameter("equipmentStatus");

            Camp c = new Camp();
            c.setCampName(campName);
            c.setSeason(season);
            c.setCampPrice(campPrice);
            c.setLocationStatus(locationStatus);
            c.setCampParticipantsNo(campParticipantsNo);
            c.setEquipmentStatus(equipmentStatus);
            
            List<Location> locats = (List) request.getSession().getAttribute("locations");
            String lo;
            for (int i = 0; i < locats.size(); i++) {
                lo = request.getParameter(locats.get(i).getLocation());
                if (lo != null) { //it means that the checkbox is checked
                    System.out.println("The location wanted is " + locats.get(i).getLocation());
                    Location locat = locationDAO.getLocation(em, locats.get(i).getLocation());
                    c.setLocation(locat);
                    locat.setCamp(c);
                }
            }

            String startDate = request.getParameter("startDate");
            System.out.println("Start date: " + startDate);
            String Sdat;

            String parts[] = startDate.split("/");
            String sday = parts[0];
            String smonth = parts[1];
            String syear = parts[2];
            SimpleDateFormat sdf = new SimpleDateFormat("dd/M/yyyy");
            Calendar calendar = new GregorianCalendar(Integer.parseInt(syear), Integer.parseInt(smonth) - 1, Integer.parseInt(sday));
            Sdat = sdf.format(calendar.getTime());
            System.out.println("Start date: " + Sdat);

            String endDate = request.getParameter("endDate");
            String Edat;

            String parts2[] = endDate.split("/");
            String eday = parts2[0];
            String emonth = parts2[1];
            String eyear = parts2[2];
            sdf = new SimpleDateFormat("dd/M/yyyy");
            calendar = new GregorianCalendar(Integer.parseInt(eyear), Integer.parseInt(emonth) - 1, Integer.parseInt(eday));
            Edat = sdf.format(calendar.getTime());
            System.out.println("End date: " + Edat);
            c.setStartDate(Sdat);
            c.setEndDate(Edat);
            
            em.getTransaction().begin();

            try {

                campDAO.store(em, c);
                em.getTransaction().commit();
            } catch (Exception ex) {
                ex.printStackTrace();
                em.getTransaction().rollback();
            } finally {
                em.close();

            }
            out.println("<script language='javascript'>alert('Added camp!');"
                    + "self.location='home.jsp';</script>");
        }
    }
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
