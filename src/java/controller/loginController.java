/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import domain.Activity;
import domain.Camp;
import domain.Equipment;
import domain.Location;
import domain.Registration;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import model.ActivityDAO;
import model.CampDAO;
import model.EquipmentDAO;
import model.LocationDAO;
import model.RegistrationDAO;

/**
 *
 * @author Kristijan
 */
@WebServlet(name = "loginController", urlPatterns = {"/loginController"})
public class loginController extends HttpServlet {
    
    private RegistrationDAO registrationDAO = new RegistrationDAO();
    private EquipmentDAO equipmentDAO = new EquipmentDAO();
    private LocationDAO locationDAO = new LocationDAO();
    private CampDAO campDAO = new CampDAO();
    private ActivityDAO activityDAO = new ActivityDAO();

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    public String getHash(String password) {
        MessageDigest digest = null;
        try {
            digest = MessageDigest.getInstance("SHA-1");
        } catch (NoSuchAlgorithmException ex) {
            System.out.println("WE've got an error over here");
            ex.printStackTrace();
        }
        digest.reset();
        try {
            digest.update(password.getBytes("UTF-8"));
        } catch (UnsupportedEncodingException ex) {
            System.out.println("got you");
            ex.printStackTrace();
        }
        return new BigInteger(1, digest.digest()).toString(16);
    }

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        HttpSession session = request.getSession(true); //to create a new session

        EntityManagerFactory emf = Persistence.createEntityManagerFactory("SDM_ProjectPU");
        EntityManager em = emf.createEntityManager();
        String username;
        try (PrintWriter out = response.getWriter()) {
            boolean ok1 = false, ok2 = false;
            username = request.getParameter("uname");
            System.out.println("The username is " + username);
            String password = request.getParameter("password1");
            System.out.println("The password is " + password);
            password = getHash(password);
            System.out.println("The hashed password is " + password);

            em.getTransaction().begin();
            if (registrationDAO.checkUsername(em, username) == true) {
                ok1 = true;
            }

            if (ok1 == false) {
                out.println("<script language='javascript'>alert('This username does not exist. Try again or register'); "
                        + "self.location='register.jsp';</script>");

            }

            if (ok1 == true && (registrationDAO.findPassword(em, username).equalsIgnoreCase(password))) {
                System.out.println("The passwords are identical");
                ok2 = true;
            }

            if (ok1 == true && ok2 == false) {
                out.println("<script language='javascript'>alert('Wrong password for this username'); "
                        + "self.location='login.jsp';</script>");

            }

            if (!session.isNew() && ok2 == true) {
                System.out.println("Peanut and butter");
                session.setAttribute("uname", request.getParameter("uname"));
                Registration r = registrationDAO.getParticipant(em, username);

                List<Camp> shoppingCartList = new ArrayList<>();
                session.setAttribute("shoppingCartList", shoppingCartList);
                out.println("<script language='javascript'>alert('You got logged in');"
                        + "self.location='home.jsp';</script>");
                request.getSession().setAttribute("uname", username);
                List<Equipment> equips = equipmentDAO.displayEquipments(em);
                request.getSession().setAttribute("equipments", equips);
                List<Location> locs = locationDAO.displayLocations(em);
                request.getSession().setAttribute("locations", locs);
                List<Camp> camps = campDAO.displayCamps(em);
                request.getSession().setAttribute("camps", camps);
                List<Activity> activities = activityDAO.displayCamps(em);
                request.getSession().setAttribute("activities", activities);
            }

            em.close();
        }

    }
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
