/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import domain.Company;
import domain.Equipment;
import domain.Location;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.CompanyDAO;
import model.EquipmentDAO;
import model.LocationDAO;

/**
 *
 * @author Kristijan
 */
@WebServlet(name = "addCompanyController", urlPatterns = {"/addCompanyController"})
public class addCompanyController extends HttpServlet {

    private CompanyDAO companyDAO = new CompanyDAO();
    private EquipmentDAO equipmentDAO = new EquipmentDAO();
    private LocationDAO locationDAO = new LocationDAO();

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("SDM_ProjectPU");
        EntityManager em = emf.createEntityManager();
        try (PrintWriter out = response.getWriter()) {
            String companyName = request.getParameter("cName");
            System.out.println("The company to be added is " + companyName);
            String address = request.getParameter("address");
            String cEmail = request.getParameter("cEmail");
            String cPhoneNo = request.getParameter("cPhoneNo");

            Company c = new Company();
            c.setCompanyName(companyName);
            c.setcAddress(address);
            c.setcEmail(cEmail);
            c.setcPhoneNo(cPhoneNo);

            List<Location> locats = (List) request.getSession().getAttribute("locations");
            String lo;
            for (int i = 0; i < locats.size(); i++) {
                lo = request.getParameter(locats.get(i).getLocation());
                if (lo != null) { //it means that the checkbox is checked
                    System.out.println("The location wanted is "+locats.get(i).getLocation());
                    Location locat = locationDAO.getLocation(em, locats.get(i).getLocation());
                    c.getLocations().add(locat);
                    locat.setCompany(c);
                }
            }
            
            List<Equipment> equipms = (List) request.getSession().getAttribute("equipments");
            String eq;
            for (int i = 0; i < equipms.size(); i++) {
                eq = request.getParameter(equipms.get(i).getEquipment());
                if (eq != null) { //it means that the checkbox is checked
                    Equipment equi = equipmentDAO.getEquipment(em, equipms.get(i).getEquipment());
                    c.getEquipments().add(equi);
                    equi.setCompany(c);
                }
            }

            

            em.getTransaction().begin();

            try {

                companyDAO.store(em, c);
                em.getTransaction().commit();
            } catch (Exception ex) {
                ex.printStackTrace();
                em.getTransaction().rollback();
            } finally {
                em.close();

            }
            out.println("<script language='javascript'>alert('Added company!');"
                    + "self.location='home.jsp';</script>");
        }
    }


    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
