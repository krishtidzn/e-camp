/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import domain.Admin;
import domain.Instructor;
import domain.Owner;
import domain.Participant;
import domain.Registration;
import domain.School;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Map;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.AdminDAO;
import model.InstructorDAO;
import model.OwnerDAO;
import model.ParticipantDAO;
import model.RegistrationDAO;
import model.SchoolDAO;

/**
 *
 * @author Kristijan
 */
@WebServlet(name = "registerController", urlPatterns = {"/registerController"})
public class registerController extends HttpServlet {
    
    private ParticipantDAO participantDAO = new ParticipantDAO();
    private SchoolDAO schoolDAO = new SchoolDAO();
    private RegistrationDAO registrationDAO = new RegistrationDAO();
    private AdminDAO adminDAO = new AdminDAO();
    private OwnerDAO ownerDAO = new OwnerDAO();
    private InstructorDAO instructorDAO = new InstructorDAO();

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    public String getHash(String password) {
        MessageDigest digest=null;
        try {
            digest = MessageDigest.getInstance("SHA-1");
        } catch (NoSuchAlgorithmException ex) {
            System.out.println("WE've got an error over here");
            ex.printStackTrace();
        }
        digest.reset();
        try {
            digest.update(password.getBytes("UTF-8"));
        } catch (UnsupportedEncodingException ex) {
            System.out.println("got you");
            ex.printStackTrace();
        }
        return new BigInteger(1, digest.digest()).toString(16);
    }

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("SDM_ProjectPU");
        EntityManager em = emf.createEntityManager();
        try (PrintWriter out = response.getWriter()) {
            boolean ok = true;
            String n = request.getParameter("LName");
            if (n.equals("")) {
                ok = false;
            }
            String usrname = request.getParameter("uname");
            String p1 = request.getParameter("password1");
            String p2 = request.getParameter("repassword");
            if (!p1.equals(p2)) {
                ok = false;
            }
            String e = request.getParameter("email");
            if (e.equals("")) {
                ok = false;
            }

            if (ok == false) {
                out.println("<a href=\"register.jsp\">Register again</a>");
            }

            if (ok == true) {
                Map<String, String[]> map = request.getParameterMap();
                int i = 0;
                String s;
                String idcard = null, CNP = null, address = null, schoolName = null, sAddress = null, IBAN = null, city = null, uname = null, password = null, fname = null, lname = null, email = null, gender = null, tel = null, country = null, spam = null;
                for (Map.Entry<String, String[]> entry : map.entrySet()) {
                    String name = entry.getKey();
                    String[] values = entry.getValue();

                    if (i == 0) {
                        fname = (s = Arrays.toString(values)).substring(1, s.length() - 1);
                    }

                    if (i == 1) {
                        lname = (s = Arrays.toString(values)).substring(1, s.length() - 1);
                    }

                    if (i == 2) {
                        CNP = (s = Arrays.toString(values)).substring(1, s.length() - 1);
                    
                    }

                    if (i == 3) {
                        idcard = (s = Arrays.toString(values)).substring(1, s.length() - 1);
                    }

                    if (i == 4) {
                        tel = (s = Arrays.toString(values)).substring(1, s.length() - 1);
                    }

                    if (i == 5) {
                        email = (s = Arrays.toString(values)).substring(1, s.length() - 1);
                    }

                    if (i == 6) {
                        address = (s = Arrays.toString(values)).substring(1, s.length() - 1);
                    }

                    if (i == 7) {
                        schoolName = (s = Arrays.toString(values)).substring(1, s.length() - 1);
                    }

                    if (i == 8) {
                        sAddress = (s = Arrays.toString(values)).substring(1, s.length() - 1);
                    }

                    if (i == 9) {
                        city = (s = Arrays.toString(values)).substring(1, s.length() - 1);
                    }

                    if (i == 10) {
                        uname = (s = Arrays.toString(values)).substring(1, s.length() - 1);
                    }

                    if (i == 11) {
                        password = (s = Arrays.toString(values)).substring(1, s.length() - 1);
                    }

                    ++i;

                }

                em.getTransaction().begin();
                
                System.out.println("The first 4 letters from the username are: " + usrname.substring(0, 4));

                if (registrationDAO.checkUsername(em, usrname) == true) {

                    out.println("<script language='javascript'>alert('The username already exists');"
                            + "self.location='home.jsp';</script>");
                } else 

                    if (usrname.contentEquals("admin")) {
                        Admin admin = new Admin();
                        admin.setFName(fname);
                        admin.setLName(lname);
                        admin.setCNP(CNP);
                        admin.setIDcard(idcard);
                        admin.setPhoneNo(tel);
                        admin.setEmail(email);
                        admin.setAddress(address);

                        
                        Registration r = new Registration();
                        r.setUsername(usrname);
                        System.out.println("The password to be hashed is "+password);

                        password = getHash(password);

                        r.setPassword(password);

                        Calendar calendar2 = new GregorianCalendar();
                        SimpleDateFormat sdf = new SimpleDateFormat("dd/M/yyyy");
                        String dat = sdf.format(calendar2.getTime());
                        r.setRegistrationDate(dat);

                        admin.setRegistration(r);
                        r.setAdmin(admin);

                        try {
                            adminDAO.store(em, admin);
                            registrationDAO.store(em, r);
                            em.getTransaction().commit();
                        } catch (Exception ex) {
                            ex.printStackTrace();
                            em.getTransaction().rollback();
                        } finally {
                            em.close();

                        }
                    }

                    else 
                if (usrname.substring(0, 4).contentEquals("inst")) {
                    Instructor instr = new Instructor();
                        instr.setFName(fname);
                        instr.setLName(lname);
                        instr.setCNP(CNP);
                        instr.setIDcard(idcard);
                        instr.setPhoneNo(tel);
                        instr.setEmail(email);
                        instr.setAddress(address);

                        
                        Registration r = new Registration();
                        r.setUsername(usrname);

                        password = getHash(password);

                        r.setPassword(password);

                        Calendar calendar2 = new GregorianCalendar();
                        SimpleDateFormat sdf = new SimpleDateFormat("dd/M/yyyy");
                        String dat = sdf.format(calendar2.getTime());
                        r.setRegistrationDate(dat);

                        instr.setRegistration(r);
                        r.setInstructor(instr);

                        try {
                            instructorDAO.store(em, instr);
                            registrationDAO.store(em, r);
                            em.getTransaction().commit();
                        } catch (Exception ex) {
                            ex.printStackTrace();
                            em.getTransaction().rollback();
                        } finally {
                            em.close();

                        }
                    } else if (usrname.contentEquals("owner")) {
                        Owner o = new Owner();
                        o.setFName(fname);
                        o.setLName(lname);
                        o.setCNP(CNP);
                        o.setIDcard(idcard);
                        o.setPhoneNo(tel);
                        o.setEmail(email);
                        o.setAddress(address);

                        
                        Registration r = new Registration();
                        r.setUsername(usrname);

                        password = getHash(password);

                        r.setPassword(password);

                        Calendar calendar2 = new GregorianCalendar();
                        SimpleDateFormat sdf = new SimpleDateFormat("dd/M/yyyy");
                        String dat = sdf.format(calendar2.getTime());
                        r.setRegistrationDate(dat);

                        o.setRegistration(r);
                        r.setOwner(o);

                        try {
                            ownerDAO.store(em, o);
                            registrationDAO.store(em, r);
                            em.getTransaction().commit();
                        } catch (Exception ex) {
                            ex.printStackTrace();
                            em.getTransaction().rollback();
                        } finally {
                            em.close();

                        }

                    } else {
                        Participant participant = new Participant();
                        participant.setFName(fname);
                        participant.setLName(lname);
                        participant.setCNP(CNP);
                        participant.setIDcard(idcard);
                        participant.setPhoneNo(tel);
                        participant.setEmail(email);
                        participant.setAddress(address);

                        School school = new School();
                        school.setSchoolName(schoolName);
                        school.setsAddress(sAddress);
                        school.setCity(city);

                        school.setParticipant(participant);
                        participant.setSchool(school);

                        Registration r = new Registration();
                        r.setUsername(usrname);

                        password = getHash(password);

                        r.setPassword(password);

                        Calendar calendar2 = new GregorianCalendar();
                        SimpleDateFormat sdf = new SimpleDateFormat("dd/M/yyyy");
                        String dat = sdf.format(calendar2.getTime());
                        r.setRegistrationDate(dat);

                        participant.setRegistration(r);
                        r.setParticipant(participant);

                        try {
                            participantDAO.store(em, participant);
                            schoolDAO.store(em, school);
                            registrationDAO.store(em, r);
                            em.getTransaction().commit();
                        } catch (Exception ex) {
                            ex.printStackTrace();
                            em.getTransaction().rollback();
                        } finally {
                            em.close();

                        }
                    }
                    out.println("<script language='javascript'>alert('User added succesfully'); "
                            + "self.location='home.jsp';</script>");
                }
            //}
        }
    }
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
