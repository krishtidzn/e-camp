/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import domain.Bill;
import domain.Camp;
import domain.Participant;
import domain.Purchase;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import model.BillDAO;
import model.CampDAO;
import model.ParticipantDAO;
import model.PurchaseDAO;

/**
 *
 * @author Kristijan
 */
@WebServlet(name = "bookCapmController", urlPatterns = {"/bookCapmController"})
public class bookCapmController extends HttpServlet {

    private PurchaseDAO purchaseDAO = new PurchaseDAO();
    private ParticipantDAO participantDAO = new ParticipantDAO();
    private CampDAO campDAO = new CampDAO();
    private BillDAO billDAO = new BillDAO();

    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("SDM_ProjectPU");
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        HttpSession session = request.getSession();
        try (PrintWriter out = response.getWriter()) {
            
            if (session.getAttribute("uname") == null) {
                out.println("<script language='javascript'>alert('You have to login in order to book a camp!');"
                        + "self.location='http://localhost:9999/SDM_Project/home.jsp';</script>");
            } else {
                String uname = session.getAttribute("uname").toString();
                Participant p = participantDAO.retrieveParticipant(em, uname);
                Calendar calendar2 = new GregorianCalendar();
                SimpleDateFormat sdf = new SimpleDateFormat("dd/M/yyyy");
                String dat = sdf.format(calendar2.getTime());
                System.out.println("The Camp with the campName has to be brought " + request.getParameter("campName"));
                Camp c = campDAO.getCamp(em, request.getParameter("campName"));
                Purchase purchase = new Purchase();
                Bill bill = new Bill();
                purchase.setPurchaseDate(dat);
                              

                bill.setTotalSum(c.getCampPrice());
                //bill.setTotalSum(totalSum);
                
                purchase.setParticipant(p);
                c.setPurchase(purchase);
                purchase.setBill(bill);
                bill.setPurchase(purchase);
                c.setCampParticipantsNo(c.getCampParticipantsNo()-1);
                p.setCamp(c);
                p.setPurchase(purchase);
                purchase.setCamp(c);
                
                try {
                    billDAO.store(em, bill);
                    purchaseDAO.store(em, purchase);
                    em.getTransaction().commit();
                } catch (Exception ex) {
                    ex.printStackTrace();
                    em.getTransaction().rollback();
                } finally {
                    em.close();

                }

                out.println("<script language='javascript'>alert('Thank you for booking the camp! We are looking forward to seeing you soon!');"
                        + "self.location='http://localhost:9999/SDM_Project/home.jsp';</script>");
            }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
