/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import domain.Activity;
import domain.Camp;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.ActivityDAO;
import model.CampDAO;

/**
 *
 * @author Kristijan
 */
@WebServlet(name = "manageCampController", urlPatterns = {"/manageCampController"})
public class manageCampController extends HttpServlet {
    private CampDAO campDAO = new CampDAO();
    private ActivityDAO activityDAO = new ActivityDAO();

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("SDM_ProjectPU");
        EntityManager em = emf.createEntityManager();
        try (PrintWriter out = response.getWriter()) {
            em.getTransaction().begin();
            Camp camp = null;
            List<Camp> cmps = (List) request.getSession().getAttribute("camps");
            String cp;
            for (int i = 0; i < cmps.size(); i++) {
                cp = request.getParameter(cmps.get(i).getCampName());
                if (cp != null) { //it means that the checkbox is checked
                    System.out.println("The camp wanted is " + cmps.get(i).getCampName());
                    camp = campDAO.getCamp(em, cmps.get(i).getCampName());
                }
            }
            Activity act = null;
            List<Activity> acts = (List) request.getSession().getAttribute("activities");
            String ac;
            for (int i = 0; i < acts.size(); i++) {
                ac = request.getParameter(acts.get(i).getDescription());
                if (ac != null) { //it means that the checkbox is checked
                    System.out.println("The activity wanted is " + acts.get(i).getDescription());
                    act = activityDAO.getActivity(em, acts.get(i).getDescription());
                }
            }

            em.close();
            out.println("<script language='javascript'>alert('Done link!');"
                    + "self.location='home.jsp';</script>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
