/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

/**
 *
 * @author Kristijan
 */
@Entity
public class Camp implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @OneToOne(mappedBy="camp", cascade = CascadeType.PERSIST)
    private Location location;
    
    @OneToMany(mappedBy="camp", cascade = CascadeType.PERSIST)
    private List<Picture> pictures = new ArrayList<>();
    
    @OneToOne(mappedBy="camp", cascade = CascadeType.PERSIST)
    private Participant participant;
    
    @OneToOne(mappedBy="camp", cascade = CascadeType.PERSIST)
    private Purchase purchase;
    
    @OneToMany(mappedBy="camp", cascade = CascadeType.PERSIST)
    private List<Team> teams = new ArrayList<>();
    
    @OneToMany(cascade = CascadeType.PERSIST)
    private List<Activity> activities = new ArrayList<>();
    
    @ManyToMany(cascade = CascadeType.PERSIST)
    private List<Instructor> instructors = new ArrayList<>();
    
    private String season;
    private String campName;
    private double campPrice;
    private String locationStatus;
    private String startDate;
    private String endDate;
    private int campParticipantsNo;
    private String equipmentStatus;

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public List<Picture> getPictures() {
        return pictures;
    }

    public void setPictures(List<Picture> pictures) {
        this.pictures = pictures;
    }

    public Participant getParticipant() {
        return participant;
    }

    public void setParticipant(Participant participant) {
        this.participant = participant;
    }

    public Purchase getPurchase() {
        return purchase;
    }

    public void setPurchase(Purchase purchase) {
        this.purchase = purchase;
    }

    public List<Team> getTeams() {
        return teams;
    }

    public void setTeams(List<Team> teams) {
        this.teams = teams;
    }

    public List<Activity> getActivities() {
        return activities;
    }

    public void setActivities(List<Activity> activities) {
        this.activities = activities;
    }

    public List<Instructor> getInstructors() {
        return instructors;
    }

    public void setInstructors(List<Instructor> instructors) {
        this.instructors = instructors;
    }

   

    public String getSeason() {
        return season;
    }

    public void setSeason(String season) {
        this.season = season;
    }

    public String getCampName() {
        return campName;
    }

    public void setCampName(String campName) {
        this.campName = campName;
    }

    public double getCampPrice() {
        return campPrice;
    }

    public void setCampPrice(double campPrice) {
        this.campPrice = campPrice;
    }

    public String getLocationStatus() {
        return locationStatus;
    }

    public void setLocationStatus(String locationStatus) {
        this.locationStatus = locationStatus;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public int getCampParticipantsNo() {
        return campParticipantsNo;
    }

    public void setCampParticipantsNo(int campParticipantsNo) {
        this.campParticipantsNo = campParticipantsNo;
    }

    public String getEquipmentStatus() {
        return equipmentStatus;
    }

    public void setEquipmentStatus(String equipmentStatus) {
        this.equipmentStatus = equipmentStatus;
    }
    
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Camp)) {
            return false;
        }
        Camp other = (Camp) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "domain.Camp[ id=" + id + " ]";
    }
    
}