/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package domain;

import java.io.Serializable;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

/**
 *
 * @author Kristijan
 */
@Entity
public class Location implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    
    @ManyToOne(cascade = CascadeType.PERSIST)
    private Admin admin;
    
    @OneToOne(cascade = CascadeType.PERSIST)
    private Camp camp;
    
    @ManyToOne(cascade = CascadeType.PERSIST)
    private Company company;
    
    private String location;
    private double summerHirePrice;
    private double winterHirePrice;
    private String description;
    private int capacity;

    public Admin getAdmin() {
        return admin;
    }

    public void setAdmin(Admin admin) {
        this.admin = admin;
    }

    public Camp getCamp() {
        return camp;
    }

    public void setCamp(Camp camp) {
        this.camp = camp;
    }

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public double getSummerHirePrice() {
        return summerHirePrice;
    }

    public void setSummerHirePrice(double summerHirePrice) {
        this.summerHirePrice = summerHirePrice;
    }

    public double getWinterHirePrice() {
        return winterHirePrice;
    }

    public void setWinterHirePrice(double winterHirePrice) {
        this.winterHirePrice = winterHirePrice;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getCapacity() {
        return capacity;
    }

    public void setCapacity(int capacity) {
        this.capacity = capacity;
    }
    
}
